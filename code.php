<?php
/*
In code.php, create a function named getFullAddress() that will take four arguments:
    - Country
    - City
    - Province
    - Specific Address (such as block, lot, or building name and room number).
*/
function getFullAddress($street, $province, $city, $country){
return "$street, $province, $city, $country";
};


function getLetterGrade($grade)
{
    if ($grade >= 98 &&  $grade <= 98) {
        return "$grade is equivalent to A+";
    } else if($grade >= 95 &&  $grade <= 97) {
        return "$grade is equivalent to A";
    } else if ($grade >= 92 && $grade <= 94) {
        return "$grade is equivalent to A-";
    } else if ($grade >= 89 &&  $grade <= 91) {
        return "$grade is equivalent to B+";
    } else if ($grade >= 86 && $grade <= 88) {
        return "$grade is equivalent to B";
    } else if ($grade >= 83 && $grade <= 85) {
        return "$grade is equivalent to B-";
    } else if ($grade >=80 &&  $grade <= 82) {
        return "$grade is equivalent to C+";
    } else if ($grade >= 77 && $grade <= 79) {
        return "$grade is equivalent to C";
    } else if ($grade >= 75 && $grade <= 76) {
        return "$grade is equivalent to C-";
    } else if ($grade >= 74) {
        return "$grade is equivalent to D";
    } else {
        return "$grade can't be higher than 100 or lower than 0";
    }
};